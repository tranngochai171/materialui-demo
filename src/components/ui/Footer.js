import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import Hidden from "@material-ui/core/Hidden";
import footerAdornment from "../../assets/Footer Adornment.svg";
import facebook from "../../assets/facebook.svg";
import twitter from "../../assets/twitter.svg";
import instagram from "../../assets/instagram.svg";

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: theme.palette.common.blue,
    width: "100%",
    zIndex: theme.zIndex.modal + 1,
    position: "relative",
  },
  adornment: {
    width: "25em",
    verticalAlign: "bottom",
    [theme.breakpoints.down("md")]: {
      width: "21em",
    },
    [theme.breakpoints.down("xs")]: {
      width: "15em",
    },
  },
  gridContainer: {
    position: "absolute",
  },
  link: {
    color: "white",
    fontFamily: "Arial",
    fontSize: "0.75rem",
    fontWeight: "bold",
    textDecoration: "none",
  },
  gridItem: {
    margin: "3em",
  },
  socialContainer: {
    position: "absolute",
    // bottom: "1em",
    marginTop: "-6em",
    right: "1.5em",
    [theme.breakpoints.down("xs")]: {
      right: "0.6em",
    },
  },
  icon: {
    width: "4em",
    height: "4em",
    [theme.breakpoints.down("xs")]: {
      width: "2.5em",
      height: "2.5em",
    },
  },
}));

export default function Footer(props) {
  const classes = useStyles();
  const { value, setValue, selectedIndex, setSelectedIndex } = props;
  return (
    <footer className={classes.footer}>
      <Hidden mdDown>
        <Grid container className={classes.gridContainer} justify="center">
          <Grid item className={classes.gridItem}>
            <Grid container direction="column" spacing={2}>
              <Grid
                onClick={() => {
                  setValue(0);
                }}
                component={Link}
                to="/"
                className={classes.link}
                item
              >
                Home
              </Grid>
            </Grid>
          </Grid>
          <Grid item className={classes.gridItem}>
            <Grid container direction="column" spacing={2}>
              <Grid
                onClick={() => {
                  setValue(1);
                  setSelectedIndex(0);
                }}
                component={Link}
                to="/services"
                className={classes.link}
                item
              >
                Services
              </Grid>
              <Grid
                onClick={() => {
                  setValue(1);
                  setSelectedIndex(1);
                }}
                component={Link}
                to="/customsoftware"
                className={classes.link}
                item
              >
                Custom Software Development
              </Grid>
              <Grid
                onClick={() => {
                  setValue(1);
                  setSelectedIndex(2);
                }}
                component={Link}
                to="/mobileapps"
                className={classes.link}
                item
              >
                iOS/Android App Development
              </Grid>
              <Grid
                onClick={() => {
                  setValue(1);
                  setSelectedIndex(3);
                }}
                component={Link}
                to="/websites"
                className={classes.link}
                item
              >
                Website Development
              </Grid>
            </Grid>
          </Grid>
          <Grid item className={classes.gridItem}>
            <Grid container direction="column" spacing={2}>
              <Grid
                onClick={() => {
                  setValue(2);
                }}
                component={Link}
                to="/revolution"
                className={classes.link}
                item
              >
                The Revolution
              </Grid>
              <Grid
                onClick={() => {
                  setValue(2);
                }}
                component={Link}
                to="/revolution"
                className={classes.link}
                item
              >
                Vision
              </Grid>
              <Grid
                onClick={() => {
                  setValue(2);
                }}
                component={Link}
                to="/revolution"
                className={classes.link}
                item
              >
                Technology
              </Grid>
              <Grid
                onClick={() => {
                  setValue(2);
                }}
                component={Link}
                to="/revolution"
                className={classes.link}
                item
              >
                Process
              </Grid>
            </Grid>
          </Grid>
          <Grid item className={classes.gridItem}>
            <Grid container direction="column" spacing={2}>
              <Grid
                onClick={() => {
                  setValue(3);
                }}
                component={Link}
                to="/about"
                className={classes.link}
                item
              >
                About Us
              </Grid>
              <Grid
                onClick={() => {
                  setValue(3);
                }}
                component={Link}
                to="/about"
                className={classes.link}
                item
              >
                History
              </Grid>
              <Grid
                onClick={() => {
                  setValue(3);
                }}
                component={Link}
                to="/about"
                className={classes.link}
                item
              >
                Team
              </Grid>
            </Grid>
          </Grid>
          <Grid item className={classes.gridItem}>
            <Grid container direction="column" spacing={2}>
              <Grid
                onClick={() => {
                  setValue(4);
                }}
                component={Link}
                to="/contact"
                className={classes.link}
                item
              >
                Contact Us
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Hidden>

      <img
        className={classes.adornment}
        alt="black decorative slash"
        src={footerAdornment}
      />
      <Grid
        container
        className={classes.socialContainer}
        justify="flex-end"
        spacing={2}
      >
        <Grid
          item
          component={"a"}
          href="https://www.facebook.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img className={classes.icon} alt="facebook logo" src={facebook} />
        </Grid>
        <Grid
          item
          component={"a"}
          href="https://www.twitter.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img className={classes.icon} alt="twitter logo" src={twitter} />
        </Grid>
        <Grid
          item
          component={"a"}
          href="https://www.instagram.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img className={classes.icon} alt="instagram logo" src={instagram} />
        </Grid>
      </Grid>
    </footer>
  );
}
