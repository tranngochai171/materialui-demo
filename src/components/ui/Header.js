import React, { useState, useEffect, useMemo } from "react";
import AppBar from "@material-ui/core/AppBar";
import ToolBar from "@material-ui/core/ToolBar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { makeStyles } from "@material-ui/styles";
import logo from "../../assets/logo.svg";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

function ElevationScroll(props) {
  const { children } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

const useStyles = makeStyles((theme) => ({
  appbar: { zIndex: theme.zIndex.modal + 1 },
  toolBarMargin: {
    ...theme.mixins.toolbar,
    marginBottom: "3em",
    [theme.breakpoints.down("md")]: {
      marginBottom: "2em",
    },
    [theme.breakpoints.down("xs")]: {
      marginBottom: "0.5em",
    },
  },
  logo: {
    height: "8em",
    [theme.breakpoints.down("md")]: {
      height: "7em",
    },
    [theme.breakpoints.down("xs")]: {
      height: "5.5em",
    },
  },
  logoContainer: {
    padding: 0,
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  tabContainer: {
    marginLeft: "auto",
  },
  tab: {
    ...theme.typography.tab,
    minWidth: 10,
    marginLeft: "25px",
  },
  button: {
    ...theme.typography.estimateBtn,
    borderRadius: "50px",
    marginLeft: "50px",
    marginRight: "25px",
    height: "45px",
    "&:hover": {
      backgroundColor: theme.palette.secondary.light,
    },
  },
  menu: {
    backgroundColor: theme.palette.common.blue,
    color: "white",
    borderRadius: "0px",
  },
  menuItem: {
    ...theme.typography.tab,
    opacity: 0.7,
    "&:hover": {
      opacity: 1,
    },
  },
  drawerIconContainer: {
    marginLeft: "auto",
    marginRight: "25px",
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  drawerIcon: {
    height: "50px",
    width: "50px",
  },
  drawer: {
    backgroundColor: theme.palette.common.blue,
  },
  drawerItem: {
    ...theme.typography.tab,
    color: "white",
    opacity: 0.7,
  },
  drawerItemEstimate: {
    backgroundColor: theme.palette.common.orange,
    "&:hover": {
      backgroundColor: theme.palette.common.orange,
    },
  },
  drawerItemSelected: { opacity: 1 },
}));

function Header(props) {
  const { value, setValue, selectedIndex, setSelectedIndex } = props;
  const classes = useStyles();
  const theme = useTheme();
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
  const matches = useMediaQuery(theme.breakpoints.down("md"));

  const [openDrawer, setOpenDrawer] = useState(false);

  const [anchorEl, setAnchorEl] = useState(null);
  const [openMenu, setOpenMenu] = useState(false);

  const tabs = useMemo(
    () => [
      { label: "Home", to: ["/"] },
      {
        label: "Services",
        to: ["/services", "/customsoftware", "/mobileapps", "/websites"],
      },
      { label: "The Revolution", to: ["/revolution"] },
      { label: "About Us", to: ["/about"] },
      { label: "Contact Us", to: ["/contact"] },
    ],
    []
  );

  const menuOptions = [
    { name: "Services", link: "/services" },
    { name: "Custom Software Development ", link: "/customsoftware" },
    { name: "iOS/Android App Development", link: "/mobileapps" },
    { name: "Website Development", link: "/websites" },
  ];

  const handleMenuItemClick = (e, index) => {
    setSelectedIndex(index);
    setValue(1);
    handleClose();
  };

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
    setOpenMenu(true);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setOpenMenu(false);
  };

  const handleChange = (e, index) => {
    setValue(index);
  };
  useEffect(() => {
    // console.log(window.location.pathname);
    let valueTemp = tabs.findIndex(({ to }) =>
      to.some((item) => item === window.location.pathname)
    );
    console.log(valueTemp);
    setValue(valueTemp >= 0 ? valueTemp : null);
    let selectedIndexTemp = tabs[1].to.findIndex(
      (path) => path === window.location.pathname
    );
    setSelectedIndex(selectedIndexTemp >= 0 ? selectedIndexTemp : null);
  }, [setSelectedIndex, setValue, tabs]);

  const tabsRender = (
    <>
      <Tabs
        value={window.location.pathname !== "/estimate" ? value : false}
        className={classes.tabContainer}
        onChange={handleChange}
        indicatorColor="primary"
      >
        {tabs.map(({ label, to: [link] }, index) => {
          if (label === "Services") {
            return (
              <Tab
                key={`${label}-${index}`}
                aria-owns={anchorEl ? "simple-menu" : undefined}
                aria-haspopup={anchorEl ? "true" : undefined}
                onMouseOver={handleClick}
                component={Link}
                to={link}
                className={classes.tab}
                label={label}
              />
            );
          }
          return (
            <Tab
              key={`${label}-${index}`}
              className={classes.tab}
              label={label}
              component={Link}
              to={link}
            />
          );
        })}
      </Tabs>
      <Button
        component={Link}
        to="/estimate"
        variant="contained"
        color="secondary"
        className={classes.button}
        onClick={() => setValue(5)}
      >
        Free Estimate
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        open={openMenu}
        onClose={handleClose}
        MenuListProps={{ onMouseLeave: handleClose }}
        style={{ zIndex: theme.zIndex.modal + 2 }}
        classes={{ paper: classes.menu }}
        elevation={0}
        keepMounted
      >
        {menuOptions.map(({ name, link }, i) => (
          <MenuItem
            component={Link}
            to={link}
            key={name}
            onClick={(e) => {
              handleMenuItemClick(e, i);
            }}
            classes={{ root: classes.menuItem }}
            selected={i === selectedIndex && value === 1}
          >
            {name}
          </MenuItem>
        ))}
      </Menu>
    </>
  );

  const drawer = (
    <>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        onOpen={() => setOpenDrawer(true)}
        classes={{ paper: classes.drawer }}
      >
        <List disablePadding>
          <div className={classes.toolBarMargin} />
          {tabs.map(({ label, to: [link] }, index) => (
            <ListItem
              key={`${label}-${index}`}
              onClick={() => {
                setOpenDrawer(false);
                setValue(index);
              }}
              divider
              button
              component={Link}
              to={link}
              selected={value === index}
            >
              <ListItemText
                className={
                  value === index
                    ? [classes.drawerItem, classes.drawerItemSelected].join(" ")
                    : classes.drawerItem
                }
                disableTypography
              >
                {label}
              </ListItemText>
            </ListItem>
          ))}
          <ListItem
            onClick={() => {
              setOpenDrawer(false);
              setValue(5);
            }}
            divider
            button
            component={Link}
            to="/estimate"
            className={classes.drawerItemEstimate}
          >
            <ListItemText
              className={
                value === 5
                  ? [classes.drawerItem, classes.drawerItemSelected].join(" ")
                  : classes.drawerItem
              }
              disableTypography
            >
              Free Estimate
            </ListItemText>
          </ListItem>
        </List>
      </SwipeableDrawer>
      <IconButton
        className={classes.drawerIconContainer}
        disableRipple
        onClick={() => setOpenDrawer(!openDrawer)}
      >
        <MenuIcon className={classes.drawerIcon} />
      </IconButton>
    </>
  );

  return (
    <>
      <ElevationScroll>
        <AppBar className={classes.appbar}>
          <ToolBar disableGutters>
            <Button
              component={Link}
              to="/"
              className={classes.logoContainer}
              onClick={() => setValue(0)}
              disableRipple
            >
              <img className={classes.logo} src={logo} alt="company logo" />
            </Button>
            {matches ? drawer : tabsRender}
          </ToolBar>
        </AppBar>
      </ElevationScroll>
      <div className={classes.toolBarMargin} />
    </>
  );
}

export default Header;
