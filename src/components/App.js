import { useState } from "react";
import Header from "./ui/Header";
import { ThemeProvider } from "@material-ui/styles";
import theme from "./ui/Theme";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Footer from "./ui/Footer";
import LadingPage from "./LadingPage";

function App() {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [value, setValue] = useState(0);
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Header
          value={value}
          setValue={setValue}
          selectedIndex={selectedIndex}
          setSelectedIndex={setSelectedIndex}
        />
        <Switch>
          <Route exact path="/" component={LadingPage} />
          <Route exact path="/services" render={() => <h1>Services</h1>} />
          <Route
            exact
            path="/customsoftware"
            render={() => <h1>Custom Software</h1>}
          />
          <Route exact path="/mobileapps" render={() => <h1>Mobile Apps</h1>} />
          <Route exact path="/websites" render={() => <h1>Websites</h1>} />
          <Route
            exact
            path="/revolution"
            render={() => <h1>The Revolution</h1>}
          />
          <Route exact path="/about" render={() => <h1>About Us</h1>} />
          <Route exact path="/contact" render={() => <h1>Contact Us</h1>} />
          <Route exact path="/estimate" render={() => <h1>Estimate</h1>} />
        </Switch>
        <Footer
          value={value}
          setValue={setValue}
          selectedIndex={selectedIndex}
          setSelectedIndex={setSelectedIndex}
        />
      </Router>
    </ThemeProvider>
  );
}

export default App;
